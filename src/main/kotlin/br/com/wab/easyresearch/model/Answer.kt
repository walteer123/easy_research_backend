package br.com.wab.easyresearch.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document
class Answer {

    @Id
    var id: String? = null

    @DBRef var question: Question? = null

    var value: String? = null //para retornos em string
    var date: Date? = null
    var values: MutableList<String> = ArrayList()//para retornos em que sao possiveis mais de uma resposta
}
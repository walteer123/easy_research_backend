package br.com.wab.easyresearch.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import java.io.Serializable

@Document
class Survey(val name:String, val description: String, @DBRef val answers: MutableList<Answer>, @DBRef val template: Template): Serializable{
    @Id
    var id: String? = null
}
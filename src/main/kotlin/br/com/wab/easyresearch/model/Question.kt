package br.com.wab.easyresearch.model


import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.io.Serializable
import java.util.ArrayList

@Document
class Question(var title: String, var type : QuestionType?, var required: Boolean = false): Serializable{

    @Id
    var id: String? = null
    var options: MutableList<String> = ArrayList()
}

enum class QuestionType{
    TEXT,
    TEXTAREA,
    NUMBER,
    SELECT,
    CHECKBOX,
    RADIO
}
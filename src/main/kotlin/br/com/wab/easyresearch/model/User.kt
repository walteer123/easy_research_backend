package br.com.wab.easyresearch.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import java.io.Serializable

@Document
class User(val userName: String, val email: String, val password: String, val firebaseUID: String,val admin: Boolean): Serializable{
    @Id
    var id: String? = null
}
package br.com.wab.easyresearch.model

import org.springframework.beans.factory.annotation.Value
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import java.io.Serializable

@Document
class Template(var name: String, var description: String, @DBRef var questions: MutableList<Question>, var createdBy: String): Serializable {
    @Id
    var id: String? = null

    var surveyUrl: String = "http://localhost:4000/createSurvey"
}
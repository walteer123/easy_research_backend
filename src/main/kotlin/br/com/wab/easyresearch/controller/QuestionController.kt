package br.com.wab.easyresearch.controller

import br.com.wab.easyresearch.model.Question
import br.com.wab.easyresearch.service.QuestionService
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin
@RequestMapping("/question")
class QuestionController(val service: QuestionService) {

    @PostMapping
    fun add(@RequestBody question: Question) = service.add(question)

    @PostMapping("/addAll")
    fun addAll(@RequestBody questionList: MutableList<Question>) = service.addAll(questionList)

}
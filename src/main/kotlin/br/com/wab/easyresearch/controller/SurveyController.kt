package br.com.wab.easyresearch.controller

import br.com.wab.easyresearch.model.Survey
import br.com.wab.easyresearch.model.User
import br.com.wab.easyresearch.service.SurveyService
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin
@RequestMapping("/survey")
class SurveyController(val service: SurveyService) {

    @PostMapping
    fun add(@RequestBody survey: Survey) = service.add(survey)

    @GetMapping("/getAll")
    fun findAll(@RequestParam("offset")offset : Int, @RequestParam("limit")limit : Int) = service.findAll(PageRequest.of(offset,limit))

    //@PostMapping("/getAll/createdBy")
    //fun findAllCreatedBy(@RequestParam("offset")offset : Int, @RequestParam("limit")limit : Int, @RequestBody user: User) = service.findAllCreatedBy(user)

    @GetMapping("/getBy/id/{id}")
    fun findById(@PathVariable("id") id: String) = service.findById(id)

    @GetMapping("/getBy/name/{name}")
    fun findByName(@PathVariable("name") name: String) = service.findByName(name)

    @GetMapping("/getAllBy/template/{templateId}")
    fun findAllByTemplateId(@PathVariable("templateId") id: String) = service.findAllByTemplateId(id)

}
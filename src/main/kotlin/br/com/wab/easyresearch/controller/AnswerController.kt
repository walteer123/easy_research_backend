package br.com.wab.easyresearch.controller

import br.com.wab.easyresearch.model.Answer
import br.com.wab.easyresearch.model.Question
import br.com.wab.easyresearch.service.AnswerService
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin
@RequestMapping("/answer")
class AnswerController(val service: AnswerService){

    @PostMapping
    fun add(@RequestBody answer: Answer) = service.add(answer)

    @PostMapping("/addAll")
    fun addAll(@RequestBody answers: MutableList<Answer>) = service.addAll(answers)

    @GetMapping("/getAllBy/question/{questionId}")
    fun findAllByQuestionId(@PathVariable("questionId") id: String) = service.findAllByQuestionId(id)

    @PostMapping("/getAllBy/question")
    fun findAllByQuestionsList(@RequestBody questionsList: MutableList<Question>) = service.findAllByQuestionsList(questionsList)

}
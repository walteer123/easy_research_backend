package br.com.wab.easyresearch.controller

import br.com.wab.easyresearch.exception.GenericExceptionHandler
import com.google.api.gax.rpc.NotFoundException
import com.google.firebase.auth.FirebaseAuthException
import com.google.gson.GsonBuilder
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.io.IOException
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException
import java.lang.RuntimeException

@ControllerAdvice
class ExceptionResponseHandler : ResponseEntityExceptionHandler(){

    @ExceptionHandler(value = [IllegalArgumentException::class, IllegalStateException::class ])
    fun handleIllegal(ex: RuntimeException, request: WebRequest): ResponseEntity<Any>{
        val message = ex.message
        return handleExceptionInternal(ex, message, HttpHeaders(), HttpStatus.BAD_REQUEST, request)
    }

    @ExceptionHandler(value = [GenericExceptionHandler.ResourceNotFoundException::class])
    fun handleNotFound(ex: RuntimeException, request: WebRequest): ResponseEntity<Any>{
        val message = ex.message
        return handleExceptionInternal(ex, message, HttpHeaders(), HttpStatus.NOT_FOUND, request)
    }

    @ExceptionHandler(value = [FirebaseAuthException::class])
    fun handleAuth(ex: FirebaseAuthException, request: WebRequest): ResponseEntity<Any>{
        val message = ex.message
        return handleExceptionInternal(ex, message, HttpHeaders(), HttpStatus.UNAUTHORIZED, request)
    }

    @ExceptionHandler(value = [IOException::class])
    fun handleIO(ex: IOException, request: WebRequest): ResponseEntity<Any>{
        val message = ex.message
        return handleExceptionInternal(ex, message, HttpHeaders(), HttpStatus.UNAUTHORIZED, request)
    }

}
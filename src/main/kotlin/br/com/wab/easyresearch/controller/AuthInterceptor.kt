package br.com.wab.easyresearch.controller

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import org.springframework.stereotype.Service
import org.springframework.web.method.HandlerMethod
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse



@Service
class AuthInterceptor: HandlerInterceptorAdapter() {

    @Throws(Exception::class)
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
//        if (getController(ALGUM_CONTROLLER::class.java, handler) != null) {
//            return true
//      }

        /*if(request.method == "OPTIONS"){
            response.setHeader("Access-Control-Allow-Origin", "*")
            response.setHeader("Content-Type", "application/x-www-form-urlencoded")
            response.setHeader("Accept", "application/json")
            response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE, PATCH")
            response.setHeader("Access-Control-Max-Age", "3600")
            response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Access-Token, Access-Control-Allow-Credentials")
//            response.setHeader("Access-Control-Expose-Headers", "Location")
            response.status = 201
            return false
        }*/

        val token = request.getHeader("Access-Token")
        if(token.isNullOrBlank()){
            val urlHost = request.getHeader("referer").split("?")[0]
            if(urlHost == "http://localhost:4000/createSurvey")
                return true
        }
        return try {
            FirebaseAuth.getInstance().verifyIdToken(token)
            true
        } catch (e: FirebaseAuthException){
            response.sendError(401, e.message)
            false
        } catch (e: Exception){
            response.sendError(401, e.message)
            false
        }
    }

    private fun <T> getController(clazz: Class<T>, handler: Any): T? {
        if (handler is HandlerMethod) {
            val bean = handler.bean
            if (clazz.isInstance(bean)) {
                return bean as T
            }
        }
        return null
    }


}
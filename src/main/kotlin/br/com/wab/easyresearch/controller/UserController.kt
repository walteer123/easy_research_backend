package br.com.wab.easyresearch.controller

import br.com.wab.easyresearch.model.User
import br.com.wab.easyresearch.service.UserService
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/user")
class UserController(val service: UserService) {

    @GetMapping("/getAll")
    fun findAll(@RequestParam("offset")offset : Int, @RequestParam("limit")limit : Int) = service.findAll(PageRequest.of(offset,limit))

    @GetMapping("/getBy/id/{id}")
    fun findById(@PathVariable("id")id: String) = service.findById(id)

    @PostMapping("/admin")
    fun addAdmin(@RequestBody user: User) = service.addAdmin(user)

    @PostMapping
    fun add(@RequestBody user: User) = service.add(user)

    @PutMapping
    fun update(@RequestBody user: User) = service.update(user)

    @DeleteMapping
    fun delete(@RequestBody user: User) = service.delete(user)



}
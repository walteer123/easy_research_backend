package br.com.wab.easyresearch.controller


import br.com.wab.easyresearch.model.Template
import br.com.wab.easyresearch.model.User
import br.com.wab.easyresearch.service.TemplateService
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin
@RequestMapping("/template")
class TemplateController(val service : TemplateService){

    @PostMapping
    fun add(@RequestBody template: Template) = service.add(template)

    @GetMapping("/getAll")
    fun findAll(@RequestParam("offset")offset : Int, @RequestParam("limit")limit : Int) = service.findAll(PageRequest.of(offset,limit))

    @GetMapping("/getAll/createdBy/{id}")
    fun findAllCreatedBy(@RequestParam("offset")offset : Int, @RequestParam("limit")limit : Int, @PathVariable("id") userId: String)
            = service.findAllCreatedBy(userId,PageRequest.of(offset,limit, Sort.by("id").descending()))

    @GetMapping("/getBy/id/{id}")
    fun findById(@PathVariable("id") id: String) = service.findById(id)

    @GetMapping("/getBy/name/{name}")
    fun findByName(@PathVariable("name") name: String) = service.findByName(name)

    @PutMapping
    fun update(@RequestBody template: Template) = service.update(template)

    @DeleteMapping
    fun delete(@RequestBody template: Template) = service.delete(template)

}
package br.com.wab.easyresearch.repository

import br.com.wab.easyresearch.model.Question
import org.springframework.data.mongodb.repository.MongoRepository

interface QuestionRepository: MongoRepository<Question, String> {
}
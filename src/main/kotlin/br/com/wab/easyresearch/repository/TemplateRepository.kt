package br.com.wab.easyresearch.repository

import br.com.wab.easyresearch.model.Template
import br.com.wab.easyresearch.model.User
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.mongodb.repository.MongoRepository

interface TemplateRepository :MongoRepository<Template,String> {

    //fun findAllByCreatedBy(pageRequest: PageRequest, user: User): Page<MutableList<Template>>
    fun findAllByCreatedBy( userId: String, pageRequest: PageRequest): Page<MutableList<Template>>

    fun findByName(name: String): Template
}
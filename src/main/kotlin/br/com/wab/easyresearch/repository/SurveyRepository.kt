package br.com.wab.easyresearch.repository

import br.com.wab.easyresearch.model.Answer
import br.com.wab.easyresearch.model.Survey
import br.com.wab.easyresearch.model.Template
import org.springframework.data.mongodb.repository.MongoRepository

interface SurveyRepository:MongoRepository<Survey, String> {

    fun findByName(name: String): Survey

    fun findAllByTemplate(template: Template): MutableList<Survey>

    fun findAllByTemplateId(id:String): Set<Survey>

}
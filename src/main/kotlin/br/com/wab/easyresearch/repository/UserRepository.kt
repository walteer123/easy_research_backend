package br.com.wab.easyresearch.repository

import br.com.wab.easyresearch.model.User
import org.springframework.data.mongodb.repository.MongoRepository

interface UserRepository:MongoRepository<User, String> {

    fun findByUserNameAndPassword(username: String, password: String): User

    fun findByUserName(username: String): User

}
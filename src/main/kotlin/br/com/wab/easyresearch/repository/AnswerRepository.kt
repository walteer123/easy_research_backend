package br.com.wab.easyresearch.repository

import br.com.wab.easyresearch.model.Answer
import br.com.wab.easyresearch.model.Question
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.repository.MongoRepository

interface AnswerRepository: MongoRepository<Answer, String> {

    fun findAllByQuestionId(id: String): MutableList<Answer>

    fun findAllByQuestionIn(questionsList: MutableList<Question>, sort: Sort): MutableList<Answer>

}
package br.com.wab.easyresearch.service

import br.com.wab.easyresearch.model.Answer
import br.com.wab.easyresearch.model.Question
import br.com.wab.easyresearch.repository.AnswerRepository
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Component

@Component
class AnswerService(val repository: AnswerRepository) {

    fun add(answer: Answer) = repository.insert(answer)

    fun addAll(answers: MutableList<Answer>) = repository.saveAll(answers)

    fun findAllByQuestionId(id: String) = repository.findAllByQuestionId(id)

    fun findAllByQuestionsList(questionsList: MutableList<Question>) = repository.findAllByQuestionIn(questionsList, Sort(Sort.Direction.ASC, "question"))



}
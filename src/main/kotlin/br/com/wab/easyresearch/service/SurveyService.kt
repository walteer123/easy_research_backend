package br.com.wab.easyresearch.service

import br.com.wab.easyresearch.model.Survey
import br.com.wab.easyresearch.repository.SurveyRepository
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Component

@Component
class SurveyService(val repository: SurveyRepository) {

    fun add(survey: Survey) = repository.insert(survey)

    fun findAll(pageRequest: PageRequest) = repository.findAll(pageRequest).content

    fun findById(id: String) = repository.findById(id)

    fun findByName(name: String) = repository.findByName(name)

    fun findAllByTemplateId(id: String) = repository.findAllByTemplateId(id)

}
package br.com.wab.easyresearch.service

import br.com.wab.easyresearch.model.User
import br.com.wab.easyresearch.repository.UserRepository
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserRecord
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Component

@Component
class UserService(val repository: UserRepository) {

    fun addAdmin(user: User){

        var request = UserRecord.CreateRequest()
        request.setDisplayName(user.userName)
        request.setEmail(user.email)
        request.setPassword(user.password)

        FirebaseAuth.getInstance().createUser(request)
        repository.insert(user)
    }

    fun add(user: User) = repository.insert(user)

    fun delete(user: User) {
        FirebaseAuth.getInstance().deleteUser(user.firebaseUID)
        repository.delete(user)
    }

    fun update(user: User) {
        var request = UserRecord.UpdateRequest(user.firebaseUID)
                .setDisplayName(user.userName)
                .setEmail(user.email)
                .setPassword(user.password)
        FirebaseAuth.getInstance().updateUser(request)
        repository.save(user)
    }

    fun findById(id: String) = repository.findById(id)

    fun findAll(pageRequest: PageRequest) = repository.findAll(pageRequest).content
}
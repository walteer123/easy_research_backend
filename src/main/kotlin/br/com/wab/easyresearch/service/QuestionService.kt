package br.com.wab.easyresearch.service

import br.com.wab.easyresearch.model.Question
import br.com.wab.easyresearch.repository.QuestionRepository
import org.springframework.stereotype.Component

@Component
class QuestionService(val repository: QuestionRepository) {

    fun add(question: Question) {

        //val mock = Checkbox("Question Mock", checked = true)
        repository.insert(question)
    }

    fun addAll(question: MutableList<Question>) = repository.saveAll(question)
}
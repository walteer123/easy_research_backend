package br.com.wab.easyresearch.service

import br.com.wab.easyresearch.exception.GenericExceptionHandler
import br.com.wab.easyresearch.model.Template
import br.com.wab.easyresearch.repository.QuestionRepository
import br.com.wab.easyresearch.repository.SurveyRepository
import br.com.wab.easyresearch.repository.TemplateRepository
import com.google.gson.GsonBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import java.lang.IllegalArgumentException
import java.lang.StringBuilder


@Component
class TemplateService(val repository: TemplateRepository) {

    @Autowired
    lateinit var surveyRepository: SurveyRepository

    @Autowired
    lateinit var questionRepository: QuestionRepository

    fun add(template: Template): ResponseEntity<Template>{

        return try {
            var data = repository.save(template)

            if (!data.id.isNullOrBlank()){
                data.surveyUrl += "?templateId=${data.id}"
                data = repository.save(data)
            }
            ResponseEntity.ok(data)
        } catch (e: Exception){
            ResponseEntity.status(400).body(null)
        }

    }

    fun findAll(pageRequest: PageRequest) = repository.findAll(pageRequest).content

    fun findById(id: String) = repository.findById(id)

    fun findAllCreatedBy(userId: String, pageRequest: PageRequest): ResponseEntity<String> {
        return try {
            var templateList = repository.findAllByCreatedBy(userId, pageRequest).content
            ResponseEntity.ok(GsonBuilder()
                    .setPrettyPrinting()
                    .create()
                    .toJson(templateList))

        }catch (e: Exception){
            ResponseEntity.status(400).body(null)
        }

    }

    fun findByName(name: String) = repository.findByName(name)

    fun update(template: Template){
        val result = surveyRepository.findAllByTemplate(template)
        when {
            result.isEmpty() -> {
                questionRepository.saveAll(template.questions)
                repository.save(template)
            }
            else -> throw GenericExceptionHandler.ResourceNotAllowed("Não é possível editar! Formulário já contém respostas!")
        }

    }


    fun delete(template: Template){
        //se tiver survey deleto, depois deleto as questions e depois o template
        val surveyList = surveyRepository.findAllByTemplate(template)
        when {
            surveyList.isNotEmpty() -> surveyRepository.deleteAll(surveyList)
        }
        questionRepository.deleteAll(template.questions)
        repository.delete(template)
    }

}
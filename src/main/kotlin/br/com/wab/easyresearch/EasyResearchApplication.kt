package br.com.wab.easyresearch

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.util.ResourceUtils

@SpringBootApplication
class EasyResearchApplication

fun main(args: Array<String>) {
	runApplication<EasyResearchApplication>(*args)

	val file = ResourceUtils.getFile("classpath:firebaseAccountSettings.json")
	val options = FirebaseOptions.Builder()
			.setCredentials(GoogleCredentials.fromStream(file.inputStream()))
			.setDatabaseUrl("https://easy-research-backend.firebaseio.com")
			.build()
	FirebaseApp.initializeApp(options)


}


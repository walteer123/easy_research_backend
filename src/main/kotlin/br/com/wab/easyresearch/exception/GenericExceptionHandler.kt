package br.com.wab.easyresearch.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.lang.RuntimeException

object GenericExceptionHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    class ResourceNotFoundException(exception: String) : RuntimeException(exception)

    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    class ResourceNotAllowed(exception: String): RuntimeException(exception)

}